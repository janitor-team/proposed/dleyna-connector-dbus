dleyna-connector-dbus (0.3.0-2) unstable; urgency=low

  * QA upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update standards version to 4.5.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 13 Feb 2021 01:31:04 +0000

dleyna-connector-dbus (0.3.0-1) unstable; urgency=medium

  * QA upload.
  * Upload to unstable
  * debian/control: Switch maintainer to the Debian QA Group as the package is
    orphaned (see: #873734)
  * debian/control: Bump the libdleyna-core-1.0-dev build-dependency to match
    the last release
  * debian/control: Bump Standards-Version to 4.4.1 (no further changes)
  * debian/control: Re-add the Vcs-* fields

 -- Laurent Bigonville <bigon@debian.org>  Wed, 01 Jan 2020 14:21:36 +0100

dleyna-connector-dbus (0.3.0-0.1) experimental; urgency=medium

  * Non maintainer upload
  * New upstream version
  * debian/compat: removed
  * debian/control:
    - bump debhelper to 12, update Standards-Versions
    - remove the dbg, replaced by a dbgsym
  * debian/rules:
    - tweaked for the new debhelper version
    - convert the dbg to a dbgsym

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 04 Jun 2019 16:52:06 +0200

dleyna-connector-dbus (0.2.0-1) unstable; urgency=low

  [ Emanuele Aina ]
  * New upstream release.
    + Change publish_object() signature
  * Bump Standards-Version to 3.9.5 (no changes).

  [ Logan Rosen ]
  * Use dh-autoreconf instead of autotools-dev to fix FTBFS on ppc64el

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 30 Dec 2013 12:50:15 +0100

dleyna-connector-dbus (0.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #710235)

 -- Emanuele Aina <emanuele.aina@collabora.com>  Fri, 07 Jun 2013 14:16:28 +0200
